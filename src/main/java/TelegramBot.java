import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class TelegramBot extends TelegramLongPollingBot {

    // Метод запускается, когда в чате происходит событие
    public void onUpdateReceived(Update update) {
        // Проверяет, является ли событие сообщением, и есть ли в сообщении текст:
        if (update.hasMessage() && update.getMessage().hasText()) {
            // Чтобы ответить, ему нужно создать ответное сообщение, которое является объектом.
            // Объект возвращается из функции getHelloMessage:
            SendMessage message = this.getHelloMessage(update);
            try {
                execute(message); // Отправляет сообщение
            } catch (TelegramApiException e) {
                e.printStackTrace(); // Если ловит ошибку, выводит её в консоль
            }
        }
    }

    // Указывает имя бота
    public String getBotUsername() {
        return "iborisbot";
    }

    // Указывает токен бота
    public String getBotToken() {
        return "451220067:AAHxByDEhrAIZsZBl_42OKeId5TDk3dx-rY";
    }

    // Метод создает ответное сообщение
    // В него приходит наше событие как аргумент метода
    private SendMessage getHelloMessage(Update update) {
        String userName = update.getMessage().getFrom().getUserName(); // Из события получаем имя пользователя
        String text = "Привет, " + userName + "!"; // Составляем ответный текст

        SendMessage message = new SendMessage() // Создаем объект ответного сообщение
                .setChatId(update.getMessage().getChatId()) // Устанавливаем чат, в который нужно послать сообщение
                .setText(text); // Положили в объект ответного сообщения ответный текст

        return message; // Возвращаем объект ответного сообщения
    }
}
